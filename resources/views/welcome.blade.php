<!DOCTYPE html>
<html lang="en">
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="Booklulu | Booklulu Offers a variety of items that can be use for you books marketing like website themes, graphics and more.">
                <meta name="keywords" content="Booklulu | Booklulu Offers a variety of items that can be use for you books marketing like website themes, graphics and more.">
                <meta name="author" content="DeeDel Digital">
                <title>Booklulu - Offers a variety of items that can be use for you books marketing like website themes, graphics and more.</title>
                <link rel="apple-touch-icon" href="apple-touch-icon.png">
                <link rel="icon" href="favicon.ico">
                <link href="http://fonts.googleapis.com/css?family=Quattrocento|Oswald:700" rel="stylesheet" type="text/css">
                <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
        </head>
        <body>
                <div class="wrap">
                        <h1 class="name">
                                <strong class="logo">Booklulu</strong> Offers a variety of items that can be use for you books marketing like website themes, graphics and more.
                        </h1>
                        <em><a href="mailto:info@booklulu.com">info@booklulu.com</a></em>
                </div>
        </body>
</html>

